import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConnectFour {
    private static final char[] players = new char[] { 'R', 'G' };
    private final int width, height;
    private final char[][] grid;
    private int lastCol = -1, lastTop = -1;

    /*
     * Initialize board with . (dots)
     */
    public ConnectFour(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new char[height][];
        for (int h = 0; h < height; h++) {
            Arrays.fill(this.grid[h] = new char[width], '.');
        }
    }

    public String toString() {
        return IntStream.range(0, this.width)
                        .mapToObj(Integer::toString)
                        .collect(Collectors.joining()) + "\n" +
               Arrays.stream(this.grid)
                     .map(String::new)
                     .collect(Collectors.joining("\n"));
    }

    /*
     * Prompts user for a column, repeating until a valid choice is made.
     */
    public void chooseAndDrop(char symbol, Scanner input) {
        do {
            System.out.print("\nPlayer " + symbol + " turn: ");
            int col = input.nextInt() - 1;

            if (! (col >= 0 && col < this.width)) {
                System.out.println("Column must be between 1 and " + (this.width));
                continue;
            }
            for (int h = this.height - 1; h >= 0; h--) {
                if (this.grid[h][col] == '.') {
                    this.grid[this.lastTop=h][this.lastCol=col] = symbol;
                    return;
                }
            }

            System.out.println("Column " + col + " is full.");
        } while (true); // accept inputs until one of the conditions in "do" fails
    }

    /*
     * Detects whether last disc played was a winning move or not -> if it is, return true else return false
     */
    public boolean isWinningPlay() {
        if (this.lastCol == -1) {
            throw new IllegalStateException("No move has been made yet");
        }
        char sym = this.grid[this.lastTop][this.lastCol];
        String streak = String.format("%c%c%c%c", sym, sym, sym, sym);
        return contains(this.horizontal(), streak) ||
               contains(this.vertical(), streak) ||
               contains(this.slashDiagonal(), streak) ||
               contains(this.backslashDiagonal(), streak);
    }

    /*
     * Returns contents of the row containing the last played disc
     */
    private String horizontal() {
        return new String(this.grid[this.lastTop]);
    }

    /**
     * Returns contents of the column containing the last played disc
     */
    private String vertical() {
        StringBuilder sb = new StringBuilder(this.height);
        for (int h = 0; h < this.height; h++) {
            sb.append(this.grid[h][this.lastCol]);
        }
        return sb.toString();
    }

    /*
     * Returns contents of the "/" diagonal containing the last played disc -> (coordinates have a constant sum)
     */
    private String slashDiagonal() {
        StringBuilder sb = new StringBuilder(this.height);
        for (int h = 0; h < this.height; h++) {
            int w = this.lastCol + this.lastTop - h;
            if (w >=0 && w < this.width) {
                sb.append(this.grid[h][w]);
            }
        }
        return sb.toString();
    }

    /**
     * Returns contents of the "\" diagonal containing the last played disc -> (coordinates have a constant difference)
     */
    private String backslashDiagonal() {
        StringBuilder sb = new StringBuilder(this.height);
        for (int h = 0; h < this.height; h++) {
            int w = this.lastCol - this.lastTop + h;
            if (w >=0  && w < this.width) {
                sb.append(this.grid[h][w]);
            }
        }
        return sb.toString();
    }

    private static boolean contains(String mesh, String element) {
        return mesh.indexOf(element) >= 0;
    }

    public static void main(String[] args) {
        int height = 6, width = 7, moves = height * width; // given dimensions of the board
        try (Scanner input = new Scanner(System.in)) {
            ConnectFour board = new ConnectFour(width, height);
            System.out.println("Use 1-" + (width) + " to choose a column.");
            System.out.println(board);

            for (int player = 0; moves-- > 0; player = 1 - player) {
                char symbol = players[player];
                board.chooseAndDrop(symbol, input);
                System.out.println(board);
                if (board.isWinningPlay()) {
                    System.out.println("Player " + symbol + " wins!");
                    return;
                }
            }
            System.out.println("Game resulted in a draw. There has been no winner.");
        }
    }
}